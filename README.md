**Instruções para rodar o microserviço**

OBS: Necessário java e postgresql instalado na máquina

---

## Criar a tabela no banco de dados

Siga os seguintes passos:

1. Com um editor sql de sua preferência ou com o pgAdmin acesse o servidor local do postgres
2. Crie a base de dados com o comando:  
	**CREATE DATABASE playlist;**
3. Em seguinda entre na base de dados criada e crie a tabela com o seguinte comando:  
	**CREATE TABLE history_call (  
	  id SERIAL CONSTRAINT pk_id PRIMARY KEY,  
	  date TIMESTAMP NOT NULL,  
	  city varchar(50),  
	  lat NUMERIC,  
	  long NUMERIC,  
	  temperature NUMERIC(4,2) NOT NULL,  
	  playlist VARCHAR(50) NOT NULL  
	);**
4. Usuário e senha do banco deve ser postgres e deve estar rodando na porta 5432

---

## Executar microserviço

Siga os seguintes passos:

1. Baixe os arquivos do repositório git **https://bitbucket.org/fpmxxx/playlist/src/master/**
2. No diretório que baixou os arquivos execute o comando: **mvn clean package spring-boot:repackage**
3. Acesse o diretório **target** que foi criado
4. Execute o comando **java -jar playlist-1.0.0-SNAPSHOT.jar**
5. Execute o projeto web conforme as instruções do repositório: **https://bitbucket.org/fpmxxx/playlist-web/src/master/**