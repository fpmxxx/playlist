package com.playlist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.playlist.config.ConfigProperties;

/**
 * 
 * Classe que inicia o micro servico
 * 
 */
@SpringBootApplication
@EnableConfigurationProperties(ConfigProperties.class)
public class PlaylistApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlaylistApplication.class, args);
	}

}
