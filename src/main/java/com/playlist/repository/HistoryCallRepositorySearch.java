package com.playlist.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.playlist.domain.HistoryCall;

/**
 * Interface responsavel por operacoes com o banco de dados 
 */
@Repository
public interface HistoryCallRepositorySearch extends PagingAndSortingRepository<HistoryCall, Long> {

}
