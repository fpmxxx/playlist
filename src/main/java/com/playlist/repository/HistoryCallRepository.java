package com.playlist.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.playlist.domain.HistoryCall;

/**
 * Interface responsavel por operacoes com o banco de dados 
 */
@Repository
public interface HistoryCallRepository extends CrudRepository<HistoryCall, Long> {

}
