package com.playlist.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.playlist.enums.PlaylistEnum;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.model_objects.credentials.ClientCredentials;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;
import com.wrapper.spotify.requests.data.browse.GetCategorysPlaylistsRequest;

/**
 * Classe responsavel por acessar o servico do Spotify
 */
@Component
public class PlaylistComponent {
	
	Logger logger = LogManager.getLogger(PlaylistComponent.class);

	@Value("${config.spotify.client-id}")
	private String clientId;
	
	@Value("${config.spotify.client-secret}")
	private String clientSecret;

	/**
	 * Metodo que consulta a playlist
	 * @param playlistEnum tipo de playlist a ser consultada
	 * @return retorna a playlist de acordo com o tipo informado
	 * @throws Exception caso ocorra algum erro ao acessar o servico do spotify
	 */
	public List<String> execute(PlaylistEnum playlistEnum) throws Exception {
		
		logger.info("playlist: {}", playlistEnum);
		
		List<String> playlistSuggestion = new ArrayList<String>();
		
		try {
			
			SpotifyApi spotifyApi = new SpotifyApi.Builder()
					.setClientId(clientId)
					.setClientSecret(clientSecret)
					.build();
			
			ClientCredentialsRequest clientCredentialsRequest = spotifyApi
					.clientCredentials()
					.build();
			
			ClientCredentials clientCredentials = clientCredentialsRequest.execute();
			
			spotifyApi.setAccessToken(clientCredentials.getAccessToken());
			
			GetCategorysPlaylistsRequest getCategoryRequest = spotifyApi
					.getCategorysPlaylists(playlistEnum.getName())
					.limit(10)
					.build();
			
			Paging<PlaylistSimplified> playlistSimplifiedPaging = getCategoryRequest.execute();
			
			for (PlaylistSimplified playlist : playlistSimplifiedPaging.getItems()) {
				playlistSuggestion.add(playlist.getName());
			}
			
		} catch (Exception e) {
			logger.error("Error calling playlistComponent: " + e.getMessage());
			throw new Exception(e);
		}
		

		return playlistSuggestion;
	}

}
