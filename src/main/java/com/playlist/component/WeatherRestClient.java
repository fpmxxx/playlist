package com.playlist.component;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.playlist.dto.WeatherDTO;

/**
 * Classe responsavel por acessar o servico do OpenWeatherMaps
 */
@Component
public class WeatherRestClient {
	
	Logger logger = LogManager.getLogger(WeatherRestClient.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${config.open-weather-map.url-city}")
	private String urlCity;
	
	@Value("${config.open-weather-map.url-geo-coordinates}")
	private String urlGeoCoordinates;
	
	private static final String CITY = "%CITY%";
	private static final String LAT = "%LAT%";
	private static final String LON = "%LON%";
	
	/**
	 * Metodo que consulta a temperatura de acordo com a cidade
	 * @param city cidade informada
	 * @return temperatura de acordo com a cidade informada
	 * @throws Exception caso ocorra algum erro ao acessar o servico do OpenWeatherMaps
	 */
	public WeatherDTO getWeatherByCity(String city) throws Exception {
		logger.info("getWeatherByCity - city: {}", city);
		
		WeatherDTO dto = null;
		
		try {
			String url = StringUtils.replace(urlCity, CITY, city); 
			dto = restTemplate.getForObject(url, WeatherDTO.class);
		} catch (Exception e) {
			logger.error("Error calling getWeatherByCity: " + e.getMessage());
			throw new Exception(e);
		}
		return dto;
	}

	/**
	 * Metodo que consulta a temperatura de acordo com as coordenadas de informadas
	 * @param lat latitute da cidade a ser consultada
	 * @param lon longitude da cidade a ser consultada
	 * @return temperatura de acordo com as coordenadas informadas
	 * @throws Exception caso ocorra algum erro ao acessar o servico do OpenWeatherMaps
	 */
	public WeatherDTO getWeatherByGeoCoordinates(String lat, String lon) throws Exception {
		logger.info("getWeatherByGeoCoordinates - lat: {}, long {}", lat, lon);
		
		WeatherDTO dto = null;
		
		try {
			String url = StringUtils.replace(urlGeoCoordinates, LAT, lat);
			url = StringUtils.replace(url, LON, lon);
			dto = restTemplate.getForObject(url, WeatherDTO.class);
		} catch (Exception e) {
			logger.error("Error calling getWeatherByGeoCoordinates: " + e.getMessage());
			throw new Exception(e);
		}

		return dto;
	}
}
