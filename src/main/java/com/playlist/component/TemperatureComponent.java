package com.playlist.component;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.playlist.enums.PlaylistEnum;

/**
 * Classe responsavel por identficar o tipo de playlist de acordo com a temperatura
 */
@Component
public class TemperatureComponent {
	
	Logger logger = LogManager.getLogger(TemperatureComponent.class);

	/**
	 * Metodo que identifica o tipo de playlist
	 * @param temperature informada para pesquisar o tipo de musicas da playlist
	 * @return tipo de musicas da playlist
	 */
	public PlaylistEnum execute(BigDecimal temperature) {
		temperature = temperature.setScale(2, RoundingMode.HALF_UP);
		
		logger.info("temperature: {}", temperature);
		
		if (temperature.compareTo(new BigDecimal(30)) >= 1) {
			return PlaylistEnum.PARTY;
		} else if (temperature.compareTo(new BigDecimal(15)) >= 0 && temperature.compareTo(new BigDecimal(30.99)) <= 0) {
			return PlaylistEnum.POP;
		} else if (temperature.compareTo(new BigDecimal(10)) >= 0 && temperature.compareTo(new BigDecimal(14.99)) <= 0) {
			return PlaylistEnum.ROCK;
		} else {
			return PlaylistEnum.CLASSICAL;
		}
		
	}
}
