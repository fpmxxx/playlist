package com.playlist.domain;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

/**
 * Classe com mapeamento dos campos do banco de dados
 */
@Embeddable
public class Point {

	private BigDecimal x;
	private BigDecimal y;

	public BigDecimal getX() {
		return x;
	}

	public void setX(BigDecimal x) {
		this.x = x;
	}

	public BigDecimal getY() {
		return y;
	}

	public void setY(BigDecimal y) {
		this.y = y;
	}

}