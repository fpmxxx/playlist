package com.playlist.dto;

import java.math.BigDecimal;

/**
 * Classe responsavel pelo mapeamento dos campos
 * retornados da consulta da temperatura
 */
public class WeatherMainDTO {

	private BigDecimal temp;

	public BigDecimal getTemp() {
		return temp;
	}

	public void setTemp(BigDecimal temp) {
		this.temp = temp;
	}

}
