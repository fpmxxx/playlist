package com.playlist.dto;

/**
 * Classe responsavel pelo mapeamento dos campos
 * retornados da consulta da temperatura
 */
public class WeatherDTO {

	private WeatherMainDTO main;

	public WeatherMainDTO getMain() {
		return main;
	}

	public void setMain(WeatherMainDTO main) {
		this.main = main;
	}

}
