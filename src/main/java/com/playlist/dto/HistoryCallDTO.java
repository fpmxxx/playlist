package com.playlist.dto;

import java.io.Serializable;
import java.util.List;

import com.playlist.domain.HistoryCall;

import io.swagger.annotations.ApiModelProperty;

/**
 * Classe responsavel pelo mapeamento dos campos retornados da lista de
 * historico
 */
public class HistoryCallDTO implements Serializable {

	private static final long serialVersionUID = 5781621045764524814L;

	@ApiModelProperty(value = "Lista do historico de consultas")
	private List<HistoryCall> listHistoryCall;

	@ApiModelProperty(value = "Total de registros")
	private Integer page;

	public List<HistoryCall> getListHistoryCall() {
		return listHistoryCall;
	}

	public void setListHistoryCall(List<HistoryCall> listHistoryCall) {
		this.listHistoryCall = listHistoryCall;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

}
