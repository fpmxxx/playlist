package com.playlist.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * Classe responsavel pelo mapeamento dos campos 
 * retornados da lista de musicas da playlist
 */
public class PlaylistDTO implements Serializable {

	private static final long serialVersionUID = 4360193744736055343L;

	@ApiModelProperty(value = "Lista de músicas da playlist")
	private List<String> playlistSuggestion;

	public List<String> getPlaylistSuggestion() {
		return playlistSuggestion;
	}

	public void setPlaylistSuggestion(List<String> playlistName) {
		this.playlistSuggestion = playlistName;
	}

}
