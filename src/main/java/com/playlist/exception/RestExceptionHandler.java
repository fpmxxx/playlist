package com.playlist.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;

/**
 * Classe responsavel por formatar e apresentar mensagem de erro  
 */
@RestControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorMessage globalExceptionHandler(Exception ex, WebRequest request) {
		
		ErrorMessage message = null;
		
		if (ex.getCause() instanceof HttpClientErrorException) {
			message = new ErrorMessage(HttpStatus.NOT_FOUND.value(), "Cidade não encontrada");
		} else {
			message = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
		}
		
		return message;
	}
}
