package com.playlist.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.playlist.component.PlaylistComponent;
import com.playlist.component.TemperatureComponent;
import com.playlist.component.WeatherRestClient;
import com.playlist.domain.HistoryCall;
import com.playlist.domain.Point;
import com.playlist.dto.HistoryCallDTO;
import com.playlist.dto.PlaylistDTO;
import com.playlist.dto.WeatherDTO;
import com.playlist.enums.LocationEnum;
import com.playlist.enums.PlaylistEnum;
import com.playlist.repository.HistoryCallRepository;
import com.playlist.repository.HistoryCallRepositorySearch;

/**
 * Classe que recebe os dados da requisicao rest processa as informacoes e 
 * retorna o resultado com a lista de musicas da playlist selecionada de acordo 
 * com a temperatura da localidade 
 */
@Service
public class PlaylistService {
	
	Logger logger = LogManager.getLogger(PlaylistService.class);
	
	@Autowired
	private HistoryCallRepository historyCallRepository;
	
	@Autowired
	private HistoryCallRepositorySearch historyCallRepositorySearch;
	
	@Autowired
	private WeatherRestClient weatherRestClient;
	
	@Autowired
	private TemperatureComponent temperatureComponent;
	
	@Autowired
	private PlaylistComponent playlistComponent; 
	
	private static final String NAME = "name";
	private static final String LAT = "lat";
	private static final String LON = "long";
	
	/**
	 * 
	 * @param location
	 * @param values
	 * @return
	 * @throws Exception
	 */
	public PlaylistDTO getPlaylist(LocationEnum location, Map<String, String> values) throws Exception {
		
		logger.info("getPlaylist - option: {}, params: {}", location, values);
		
		WeatherDTO weatherDTO = null;
		String cityName = null;
		String lat = null;
		String lon = null;
		if (location.equals(LocationEnum.CITY)) {
			cityName = values.get(NAME);
			weatherDTO = weatherRestClient.getWeatherByCity(cityName);
			
		} else if (location.equals(LocationEnum.GEO_COORDINATES)) {
			lat = values.get(LAT);
			lon = values.get(LON);
			weatherDTO = weatherRestClient.getWeatherByGeoCoordinates(lat, lon);
			
		}
		
		BigDecimal temperature = weatherDTO.getMain().getTemp();
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		
		PlaylistDTO playlistDTO = new PlaylistDTO();
		List<String> playlistSuggestion = playlistComponent.execute(playlistEnum);
		playlistDTO.setPlaylistSuggestion(playlistSuggestion);
		
		saveHistory(temperature, playlistEnum.getName(), cityName, lat, lon);
		
		return playlistDTO;
	}

	/**
	 * Salva no banco de dados as chamadas realizadas
	 * @param temperature
	 * @param playlist
	 * @throws Exception
	 */
	private void saveHistory(BigDecimal temperature, String playlist, String city, String lat, String lon) throws Exception {
		logger.info("saveHistory - temperature: {}, playlist: {}, city: {}, lat: {}, long: {}", temperature, playlist, city, lat, lon);
		
		try {
			HistoryCall historyCall = new HistoryCall();
			
			if (lat != null && lon != null) {
				Point coordinates = new Point();
				coordinates.setX(new BigDecimal(lat));
				coordinates.setY(new BigDecimal(lon));
				historyCall.setCoordinates(coordinates);
			}
			
			historyCall.setDate(new Date());
			historyCall.setCity(city);
			historyCall.setTemperature(temperature);
			historyCall.setPlaylist(playlist);
			
			historyCallRepository.save(historyCall);
		} catch (Exception e) {
			logger.error("Error saving in database: " + e.getMessage());
			throw new Exception(e);
		}
	}
	
	/**
	 * Lista do historico de chamadas ao servico
	 * @param page pagina dos registros
	 * @param size quantidade de registros por vez
	 * @return lista do historico armazenado no banco de dados
	 */
	public HistoryCallDTO listHistoryCall(Integer page, Integer size) {
		logger.info("listHistoryCall - page:{}, size:{}", page, size);
		
		Pageable pageable = PageRequest.of(page, size);
		
		Page<HistoryCall> pageHistotyCall = historyCallRepositorySearch.findAll(pageable);
		
		HistoryCallDTO dto = new HistoryCallDTO();
		dto.setListHistoryCall(pageHistotyCall.getContent());
		dto.setPage(pageHistotyCall.getTotalPages());
		
		return dto;
		
	}
	
}
