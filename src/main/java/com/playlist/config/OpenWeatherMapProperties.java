package com.playlist.config;

/**
 * Classe com mapeamento de campos com parametros de 
 * configuracao para acesso a API do OpenWeatherMap
 */
public class OpenWeatherMapProperties {

	private String urlCity;
	private String urlGeoCoordinates;

	public String getUrlCity() {
		return urlCity;
	}

	public void setUrlCity(String urlCity) {
		this.urlCity = urlCity;
	}

	public String getUrlGeoCoordinates() {
		return urlGeoCoordinates;
	}

	public void setUrlGeoCoordinates(String urlGeoCoordinates) {
		this.urlGeoCoordinates = urlGeoCoordinates;
	}

}
