package com.playlist.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Classe com agrupamento de parametros de configuracao 
 * para acesso a APIs externas
 */
@Configuration
@ConfigurationProperties(prefix = "config")
public class ConfigProperties {

	private SpotifyProperties spotify;
	private OpenWeatherMapProperties openWeatherMap;

	public SpotifyProperties getSpotify() {
		return spotify;
	}

	public void setSpotify(SpotifyProperties spotify) {
		this.spotify = spotify;
	}

	public OpenWeatherMapProperties getOpenWeatherMap() {
		return openWeatherMap;
	}

	public void setOpenWeatherMap(OpenWeatherMapProperties openWeatherMap) {
		this.openWeatherMap = openWeatherMap;
	}
	
	

}
