package com.playlist.config;

/**
 * Classe com mapeamento de campos com parametros de 
 * configuracao para acesso a API do Spotity
 */
public class SpotifyProperties {

	private String clientId;
	private String clientSecret;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
}
