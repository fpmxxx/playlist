package com.playlist.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.playlist.dto.HistoryCallDTO;
import com.playlist.dto.PlaylistDTO;
import com.playlist.enums.LocationEnum;
import com.playlist.service.PlaylistService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Classe responsavel por receber uma requisicao rest
 */
@Api(value = "Playlist")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/v1/playlist")
public class PlaylistRest {

	@Autowired
	private PlaylistService playlistService;

	/*
	 * Exemplos de consulta
	 * http://localhost:8080/v1/playlist/location/CITY/?name=curitiba
	 * http://localhost:8080/v1/playlist/location/GEO_COORDINATES/?lat=-26.2286&long=-52.6706
	 * 
	 * {"playlistSuggestion":["Today's Top Hits","Pop Rising"]}
	 * 
	 * SWAGGER
	 * http://localhost:8080/swagger-ui/index.html
	 */

	/**
	 * Endpoint que recebe os parametos de localizacao para 
	 * retornar a lista de musicas de uma playlist
	 * @param location tipo de localizacao a ser pesquisada
	 * @param values valores de acordo com o tipo de localizacao a ser pesquisada
	 * @return retorna a lista de musicas de uma playlist
	 * @throws Exception caso ocorra algum erro ao acessar o servico do spotify 
	 */
	@ApiOperation(value = "Playlist pela temperatura da localidade")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Retorna uma lista de musicas da playlist"),
	    @ApiResponse(code = 500, message = "Erro ao consular"),
	})
	@GetMapping("/location/{location}")
	public ResponseEntity<PlaylistDTO> getPlaylist(
			@PathVariable LocationEnum location,
			@RequestParam Map<String, String> values) throws Exception {
		
		PlaylistDTO dto = playlistService.getPlaylist(location, values);
		return new ResponseEntity<PlaylistDTO>(dto, HttpStatus.OK);

	}
	
	/**
	 * Endpoint que retorna a lista do historico de chamadas
	 * @param page numero da pagina
	 * @param size quantidade de registros por vez
	 * @return retorna a lista do historico de chamadas
	 * @throws Exception
	 */
	@ApiOperation(value = "Historico de consultas")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Retorna uma lista do historico de consultas"),
	    @ApiResponse(code = 500, message = "Erro ao consular"),
	})
	@GetMapping("/history")
	public ResponseEntity<HistoryCallDTO> getHistory(
			@RequestParam Integer page, 
			@RequestParam Integer size) throws Exception {
		
		HistoryCallDTO dto = playlistService.listHistoryCall(page, size);
		return new ResponseEntity<HistoryCallDTO>(dto, HttpStatus.OK);
	}
	
}
