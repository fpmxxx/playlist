package com.playlist.enums;

/**
 * Enum dos tipos de musicas da playlist
 */
public enum PlaylistEnum {
	PARTY("party"), 
	POP("pop"), 
	ROCK("rock"), 
	CLASSICAL("classical");
	
	private String name;

	private PlaylistEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
