package com.playlist.enums;

/**
 * Enum dos tipos de pesquisa por localizacao 
 */
public enum LocationEnum {

	CITY, GEO_COORDINATES
	
}
