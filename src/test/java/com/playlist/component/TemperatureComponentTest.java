package com.playlist.component;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.playlist.enums.PlaylistEnum;

@SpringBootTest
class TemperatureComponentTest {
	
	@Autowired
	private TemperatureComponent temperatureComponent;
	
	@Test
	void testPlaylistParty1() {
		BigDecimal temperature = new BigDecimal(31);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.PARTY, playlistEnum);
	}
	
	@Test
	void testPlaylistParty2() {
		BigDecimal temperature = new BigDecimal(30.01);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.PARTY, playlistEnum);
	}

	@Test
	void testPlaylistPop1() {
		BigDecimal temperature = new BigDecimal(15);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.POP, playlistEnum);
	}
	
	@Test
	void testPlaylistPop2() {
		BigDecimal temperature = new BigDecimal(30);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.POP, playlistEnum);
	}
	
	@Test
	void testPlaylistPop3() {
		BigDecimal temperature = new BigDecimal(20);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.POP, playlistEnum);
	}
	
	@Test
	void testPlaylistRock1() {
		BigDecimal temperature = new BigDecimal(10);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.ROCK, playlistEnum);
	}
	
	@Test
	void testPlaylistRock2() {
		BigDecimal temperature = new BigDecimal(14.58);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.ROCK, playlistEnum);
	}
	
	@Test
	void testPlaylistRock3() {
		BigDecimal temperature = new BigDecimal(12);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.ROCK, playlistEnum);
	}
	
	@Test
	void testPlaylistRock4() {
		BigDecimal temperature = new BigDecimal(14);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.ROCK, playlistEnum);
	}
	
	@Test
	void testPlaylistClassical1() {
		BigDecimal temperature = new BigDecimal(9);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.CLASSICAL, playlistEnum);
	}
	
	@Test
	void testPlaylistClassical2() {
		BigDecimal temperature = new BigDecimal(9.99);
		PlaylistEnum playlistEnum = temperatureComponent.execute(temperature);
		assertEquals(PlaylistEnum.CLASSICAL, playlistEnum);
	}
}
